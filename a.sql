create table IF NOT EXISTS users(
     email char(50),
     username char (30),
     name char(30),
     picture int,
     pass char(30) check (length(pass) >8), --( pass like '[a-z] %'), --or pass like '[0-9]%[a-z]%') ,--and length(pass) >8) ,
     is_a_publisher int,
     is_a_manager int,
     private_Q_id int, 
     private_question char(50),
     private_ans char(50),
     manager_email char(50),
     now_playing_song_id int,
     Primary key (email),
     Foreign key (private_Q_id) references private_Q_list(private_id),
     Foreign key (now_playing_song_id) references songs(song_id),
     Foreign key (manager_email) references users(email)
);

create table IF NOT EXISTS user_fav_tags (
    user_email char (50),
    tag char(20),
    Foreign key (user_email) references users(email),
    Primary key (user_email,tag)
);

create table IF NOT EXISTS user_following(
    user_email char(50),
    following_user char(50),
    Foreign key (user_email) references users(email),
    Primary key (user_email,following_user)
);

create table IF NOT EXISTS user_follower(
    user_email char(50),
    follower_user char(50),
    Foreign key (user_email) references users(email),
    Primary key (user_email,follower_user)
);

create table IF NOT EXISTS private_Q_list(
    private_id int,
    question char(100),
    Primary key ( private_id)
);

create table IF NOT EXISTS liked_songs(
    user_email char(50),
    song_id int,
    Foreign key (user_email) references users(email),
    Foreign key (song_id) references songs(song_id),
    Primary key (user_email,song_id)
);


create table IF NOT EXISTS rated_songs(
    user_email char(50),
    song_id int,
    rate int,
    Foreign key (user_email) references users(email),
    Foreign key (song_id) references songs(song_id),
    Primary key (user_email,song_id)
);


create table IF NOT EXISTS rated_albums(
    user_email char(50),
    album_id int,
    rate int,
    Foreign key (user_email) references users(email),
    Foreign key (album_id) references album(album_id),
    Primary key (user_email,album_id)
);

create table IF NOT EXISTS rated_playlists(
    user_email char(50),
    playlist_id int,
    rate int,
    Foreign key (user_email) references users(email),
    Foreign key (playlist_id) references playlist(playlist_id),
    Primary key (user_email,playlist_id)
);

create table IF NOT EXISTS listened_songs(
    user_email char(50),
    song_id int,
    Foreign key (user_email) references users(email),
    Foreign key (song_id) references songs(song_id),
    Primary key (user_email,song_id)
);

create table IF NOT EXISTS songs(
    song_id int,
    tag char(20),
    name char(30),
    time_by_sec int,
    lyrics char(500),
    shaaer char(30),
    ahangsaz  char(30),
    tahiekonande  char(30),
    maker_email char(50),
    maker_name char(30),
    maker_username char(30),
    manager_email char(50),
    album_id int,
    release_time int,
    Primary key (song_id),
    Foreign key (maker_email) references users(email),
    Foreign key (manager_email) references users(email),
    Foreign key (album_id) references album(album_id)
);

create table IF NOT EXISTS playlist_songs(
    playlist_id int,
    song_id int,
    Foreign key (playlist_id) references playlist(playlist_id),
    Foreign key (song_id) references songs(song_id),
    Primary key (song_id,playlist_id)
);

create table IF NOT EXISTS album_tags (
    album_id int,
    tag char(20),
    Foreign key (album_id) references album(album_id),
    Primary key (album_id,tag)
);

create table IF NOT EXISTS album (
    album_id int,
    num_play int,
    name char(30),
    maker_email char(50),
    maker_name char(30),
    maker_username char(30),
    release_time int,
    Primary key (album_id),
    Foreign key (maker_email) references users(email)
);


create table IF NOT EXISTS playlist (
    playlist_id int,
    playlist_name char(30),
    re_time int,
    private_public int,
    maker_email char(50),
    maker_name char(30),
    maker_username char(30),
    Primary key (playlist_id),
    Foreign key (maker_email) references users(email)
);

--how many people are registerd in the system
select count(*)
from users;

--how many songs are stored in database
select count(*)
from songs;


--how to insert new people in to system 
insert into users values('behnam20102010@gmail.com','behnam20102010','behnam',0,'1234567789',1,1,2,'who','me','dfdfdf',10);


--showing private questions
select * 
from private_Q_list;

--user private question
-- note that you have to get the mail from user
select users.private_question
from users
where users.user = 'behnam20102010@gmail.com' 

-- check if the private ans is the same 
-- if the return true, there is an existing user
select *
from users
where users.email = 'behnam20102010@gmail.com' and users.private_ans = 'me'

-- nows listing song lyrics
select songs.lyrics 
from songs,users
where songs.song_id = users.now_playing_song_id and users.email = 'behnam20102010@gmail.com'


-- made playlist by selected user with mail
select playlist.playlist_name
from playlist,users
where playlist.maker_email = users.email and users.email = 'behnam20102010@gmail.com'


-- all playlist that are not private for a given user by email
select playlist.playlist_name
from playlist
where playlist.private_public = 0
union
select playlist.playlist_name
from playlist
where playlist.private_public =1 and playlist.maker_email = 'behnam20102010@gmail.com'


--info of a selected user by email
select * 
from users
where users.email = 'behnam20102010@gmail.com'

-- liked music by seleted user
select songs.name
from songs,users,liked_songs
where liked_songs.user_email = users.email and liked_songs.song_id = songs.song_id and users.email = 'behnam20102010@gmail.com'

-- selected user, count of following
select count(*)
from users,user_following
where users.email = user_following.user_email and users.email = 'behnam20102010@gmail.com'


-- selected user, count of follower
select count(*)
from users,user_follower
where users.email = user_follower.user_email and users.email = 'behnam20102010@gmail.com'

--selected user made songs
select * 
from songs 
where songs.maker_email = 'behnam20102010@gamil.com'

-- selected user , made album
select * 
from album
where album.maker_email ='behnam20102010@gamil.com'

--listen songs by selected user
select *
from listened_songs

-- selected user, tags
select user_fav_tags.tag
from users,user_fav_tags
where users.email = user_fav_tags.user_email and users.email = 'behnam20102010@gmail.com'

--delete account with selected user email 
delete from users
where users.email = 'behnam20102010@gmail.com'

--selected user follow another user
insert into user_following
values ('myemail','behnam20102010@gmail.com');
insert into user_follower
values ('behnam20102010@gmail.com','myemail');

--insert new song into databse
insert into songs
values (1,'pop','song_name',100,'lyrics','ahang_saz','shaeer','tahiekonande','behnam20102010@gmail.com','behnam','behnam20102010','dfd',1,100)

--insert new album into database
insert into album
values (1,0,'album_name','behnam20102010@gmail.com','behnam','behnam20102010',100)

--insert a new tag for album by album-id
insert into album_tags
values(1,'pop')

--update the tag of album by album id 
update album_tags
set tag = 'new tag'
where album_id = 1


--update the tag of song
update songs
set tag = 'new tag'
where song_id=1 


-- all playlist that are not private for a given user by email
select playlist.playlist_name
from playlist
where playlist.private_public = 0
union
select playlist.playlist_name
from playlist
where playlist.private_public =1 and playlist.maker_email = 'behnam20102010@gmail.com'


--all songs
select songs.name 
from songs


--all album 
select album.name
from album


-- manager deletion with username
delete from users 
where users.username = 'behnam20102010'


--manager deletion with song name
delete from songs
where songs.name = 'song_name'


--manager songs that need approval 
select * 
from songs 
where songs.manager_email=''


--manager approval of a song 
update songs  
set manager_email='manager_email'
where songs.song_id = 2


-- manager musition that need approval 
select * 
from users
where users.is_a_publisher =1 and users.manager_email =''


--manager approval of a musition
update users
set manager_email='manager_email'
where users.is_a_publisher =1 and users.manager_email ='' and users.email='behnam20102010@gmail.com'


--time of an album by album id
select sum(songs.time_by_sec) from songs where songs.album_id=1


--add song to playlist by selected user email
insert into playlist_songs
values ((select playlist.playlist_id from playlist,users where playlist.maker_email= users.email and users.email = 'behnam20102010@gmail.com'),10) --playlist id , song id


--insert new palylist with user email
insert into playlist
values(2,'playlist_name',100,1,'behnam20102010@gmail.com',(select users.name from users where users.email = 'behnam20102010@gmail.com' ),((select users.username from users where users.email = 'behnam20102010@gmail.com' )));

--rate song by song name
insert into rated_songs
values ('behnam20102010@gmail.com',(select songs.song_id from songs where songs.name = 'given name'),2)-- 2 is and example rate

--rate song by song id
insert into rated_songs
values ('behnam20102010@gmail.com',1,2)--1 is an example song_id and  2 is an example rate


--rate song by album name
insert into rated_albums
values ('behnam20102010@gmail.com',(select album.album_id from album where album.name = 'given name'),2)-- 2 is and example rate

--rate song by song id
insert into rated_songs
values ('behnam20102010@gmail.com',1,2)--1 is an example album_id and  2 is an example rate



--rate song by playlist name
insert into rated_playlists
values ('behnam20102010@gmail.com',(select playlist.playlist_id from playlist where playlist.playlist_name = 'given name'),2)-- 2 is and example rate

--rate song by song id
insert into rated_songs
values ('behnam20102010@gmail.com',1,2)--1 is an example playlist_id and  2 is an example rate


--search songs simple
select * 
from songs
where songs.name='given_name'

--advance search songs
select * 
from songs
where songs.name = 'given_name' and maker_name = 'given maker name' and songs.tag = 'given tag' and songs.lyrics = 'given lyrics' and songs.time_by_sec < 100 and songs.release_time =100



--search  albums simple
select * 
from album
where album.name='given_name'

--advance album search
select * 
from album
where album.name = 'given_name' and 'given tag' =EXISTS(select tag from album_tags ) and album.maker_name = 'given_maker_name' and album.release_time =100

--search simple user
select * 
from users 
where users.name  = 'given_name'

--advance search user
select * 
from users 
where users.username  = 'given_username'
import { Component, OnInit } from '@angular/core';
import { Users } from 'src/app/users';
import { DbConnectionService } from 'src/app/db-connection.service';

@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: './user-edit-dialog.component.html',
  styleUrls: ['./user-edit-dialog.component.scss']
})
export class UserEditDialogComponent implements OnInit {
  user: Users;
  questions: any[] = [];
  constructor(private db: DbConnectionService) { }

  ngOnInit() {
    this.db.getAllQuestions().then((q) => {
      this.questions = q;
    });
    if (!this.user) this.user = new Users();
  }

}

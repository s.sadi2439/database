import { Injectable } from '@angular/core';
import { DbConnectionService } from './db-connection.service';

@Injectable({
  providedIn: 'root'
})
export class SocialService {

  constructor(
    private db: DbConnectionService
  ) { }

  getFollowersCount(email: string): Promise<number> {
    return this.db.getFollowersCount(email);
  }

  getFollowingCount(email: string): Promise<number> {
    return this.getFollowingCount(email);
  }
}

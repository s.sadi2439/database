export class Users {
    picture: number;
    email: string;
    username: string;
    name: string;
    pass: string;
    is_a_publisher: number;
    is_a_manager: number;
    private_Q_id: number;
    private_question: string;
    private_ans: string;
    manager_email: string;
    now_playing_song_id: number;
}

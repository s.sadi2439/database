import { Injectable } from '@angular/core';
import { DbConnectionService } from './db-connection.service';
import { Songs } from './songs';
import { AuthService } from './auth.service';
import { Album } from './album';
import { Playlist } from './playlist';

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  constructor(
    private db: DbConnectionService,
    private auth: AuthService,
  ) { }

  private randomNumber(): number {
    return parseInt((Math.random() * 1000000).toString());
  }

  public addSong(song: Songs): Promise<void> {
    song.song_id = this.randomNumber();
    song.maker_email = this.auth.user.email;
    song.maker_name = this.auth.user.name;
    song.maker_username = this.auth.user.username;
    return this.db.addSong(song);
  }

  public addAlbum(album: Album): Promise<void> {
    album.album_id = this.randomNumber();
    album.maker_email = this.auth.user.email;
    album.maker_name = this.auth.user.name;
    album.maker_username = this.auth.user.username;
    return this.db.addAlbum(album);
  }

  public addPlaylist(playlist: Playlist): Promise<void> {
    playlist.playlist_id = this.randomNumber();
    playlist.maker_email = this.auth.user.email;
    playlist.maker_name = this.auth.user.name;
    playlist.maker_username = this.auth.user.username;
    return this.db.addPlaylist(playlist);
  }

  public getUserSongs(email: string): Promise<Songs[]> {
    return this.db.getUserSongs(email);
  }
  public getUserAlbums(email: string): Promise<Album[]> {
    return this.db.getUserAlbums(email);
  }
  public getUserPlaylists(email: string): Promise<Playlist[]> {
    return this.db.getUserPlaylists(email)
  }
  public getUserSongsHistory(email: string): Promise<Songs[]> {
    return this.db.getUserSongs(email)
  }
  public getUserSongsLikes(email: string): Promise<Songs[]> {
    return this.db.getUserLikedSongs(email)  
  }
}

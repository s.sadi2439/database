import { Component, OnInit } from '@angular/core';
import { Playlist } from '../playlist';

@Component({
  selector: 'app-playlist-edit-dialog',
  templateUrl: './playlist-edit-dialog.component.html',
  styleUrls: ['./playlist-edit-dialog.component.scss']
})
export class PlaylistEditDialogComponent implements OnInit {

  playlist: Playlist;
  constructor() { }

  ngOnInit() {
    if (!this.playlist) {
      this.playlist = new Playlist();
    }
  }

}

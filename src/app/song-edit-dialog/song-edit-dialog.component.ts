import { Component, OnInit } from '@angular/core';
import { Songs } from '../songs';

@Component({
  selector: 'app-song-edit-dialog',
  templateUrl: './song-edit-dialog.component.html',
  styleUrls: ['./song-edit-dialog.component.scss']
})
export class SongEditDialogComponent implements OnInit {
  song: Songs;
  constructor() { }

  ngOnInit() {
    if (!this.song) {
      this.song = new Songs();
    }
  }

}

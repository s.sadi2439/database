import { Component } from '@angular/core';
import { DbConnectionService } from './db-connection.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private db: DbConnectionService, private auth: AuthService, private router: Router) {
    this.router.navigate(['login']);
  }
}

import { Component, OnInit } from '@angular/core';
import { Album } from '../album';

@Component({
  selector: 'app-album-edit-dialog',
  templateUrl: './album-edit-dialog.component.html',
  styleUrls: ['./album-edit-dialog.component.scss']
})
export class AlbumEditDialogComponent implements OnInit {
  album: Album;
  constructor() { }

  ngOnInit() {
    if (!this.album) {
      this.album = new Album();
    }
  }

}

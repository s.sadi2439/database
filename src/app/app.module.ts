import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import {
  MatListModule, MatIconModule, MatToolbarModule, MatTabsModule, MatCardModule, MatChipsModule, MatButtonModule, MatMenuModule, MatSelectModule, MatDialogModule, MatSnackBarModule, MatFormFieldModule, MatInputModule, MatCheckboxModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartPageComponent } from './start-page/start-page.component';
import { LayoutComponent } from './layout/layout.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ManagmentPageComponent } from './managment-page/managment-page.component';
import { MusicPageComponent } from './music-page/music-page.component';
import { LibraryPageComponent } from './library-page/library-page.component';
import { PlayListPageComponent } from './play-list-page/play-list-page.component';
import { UsersComponent } from './users/users.component';
import { AdvanceSearchComponent } from './advance-search/advance-search.component';
import { FormsModule } from '@angular/forms';
import { UserEditDialogComponent } from './user-edit-dialog/user-edit-dialog.component';
import { SongEditDialogComponent } from './song-edit-dialog/song-edit-dialog.component';
import { PlaylistEditDialogComponent } from './playlist-edit-dialog/playlist-edit-dialog.component';
import { AlbumEditDialogComponent } from './album-edit-dialog/album-edit-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    LayoutComponent,
    HomePageComponent,
    ProfilePageComponent,
    ManagmentPageComponent,
    MusicPageComponent,
    LibraryPageComponent,
    PlayListPageComponent,
    UsersComponent,
    AdvanceSearchComponent,
    UserEditDialogComponent,
    SongEditDialogComponent,
    PlaylistEditDialogComponent,
    AlbumEditDialogComponent,
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSnackBarModule,
  ],
  providers: [],
  entryComponents: [
    UserEditDialogComponent,
    SongEditDialogComponent,
    AlbumEditDialogComponent,
    PlaylistEditDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

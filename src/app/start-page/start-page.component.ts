import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DbConnectionService } from 'src/app/db-connection.service';
import { AuthService } from 'src/app/auth.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { UserEditDialogComponent } from 'src/app/user-edit-dialog/user-edit-dialog.component';
import { Users } from 'src/app/users';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss']
})
export class StartPageComponent implements OnInit {

  userCount: number;
  songCount: number;
  email: string = 'mahdi';
  password: string = '123456789';

  constructor(
    private router:Router,
    private db: DbConnectionService,
    private auth: AuthService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.db.getUserCount().then((count) => {
      this.userCount = count;
    });
    this.db.getSongCount().then((count) => {
      this.songCount = count;
    });
    
  }

  login(){
    this.auth.login(this.email, this.password).then((user) => {
      if (user) this.router.navigate(['']);
      else {
        this.snackBar.open('نام کاربری یا رمز عبور اشتباه است', '', {
          duration: 3000
        })
      }
    })
  }

  register() {
    let dialogRef = this.dialog.open(UserEditDialogComponent, {
      disableClose: true,
      direction: 'ltr',
    });

    dialogRef.afterClosed().subscribe((user: Users) => {
      this.auth.register(user).then(() => {
        this.router.navigate([''], {
          replaceUrl: true,
        });
      }, (error) => {
        this.snackBar.open(error, '', {
          duration: 3000
        })
      });
    });
  }

}

import { Injectable } from '@angular/core';
import { promise } from 'protractor';
import { Users } from 'src/app/users';
import { Songs } from './songs';
import { Playlist } from './playlist';
import { Album } from './album';

@Injectable({
  providedIn: 'root'
})
export class DbConnectionService {
  db: any;
  constructor() {
    this.db = window['openDatabase']('mydb', '1.0', 'Test DB', 2 * 1024 * 1024);
    this.init();

  }

  private init() {
    for (const table of tables) {
      this.query(table);
    }
    try {
      this.query(`insert into private_Q_list values (1, 'رنگ مورد علاقه شما چیست؟');`);
      this.query(`insert into private_Q_list values (2, 'غذای مورد علاقه شما چیست؟');`);
      this.query(`insert into private_Q_list values (3, 'نوشیدنی مورد علاقه شما چیست؟');`);
      this.query(`insert into private_Q_list values (4, 'تیم مورد علاقه شما چیست؟');`);
    } catch(ex) { }
  }

  insertUser(name: string, family: string, age: number): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.query('insert into user values (?, ?, ?)', [name, family, age]).then(() => {
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  }

//#######follow a new user
  followUser(email_main: string, email_follow: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.query('insert into user_following values (?,?); insert into user_follower values (?,?);', [email_main, email_follow, email_follow,email_main]).then(() => {
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  }


  public getAllUsers() {
  }

  private query(queryString: string, data?: any[], enableLog?: boolean): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.db.transaction((transaction) => {
        transaction.executeSql(queryString, data, (transaction, results) => {
          if (enableLog) {
            console.log('DB: ', queryString, results);
          }
          resolve(results);
        }, (transaction, error) => {
          // console.error(error);
          reject(error);
        });
      });
    });
  }




  public getUserCount(): Promise<number> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select count(*) as count from users;`, []).then(x => {
        resolve(x.rows[0].count);
      });
    });
  }
  public getSongCount(): Promise<number> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select count(*) as count from songs;`, []).then(x => {
        resolve(x.rows[0].count);
      });
    });
  }
  public getFollowingCount(email: string): Promise<number> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select count(*) as count from user_following where user_following.user_email = ?`, [email]).then(x => {
        resolve(x.rows[0].count);
      });
    });
  }
  public getFollowersCount(email: string): Promise<number> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select count(*) as count from user_follower where user_follower.user_email = ?`, [email]).then(x => {
        resolve(x.rows[0].count);
      });
    });
  }
  public register(user: Users): Promise<void> {
    
    return new Promise<any>((resolve, reject) => {
      

      this.query(`insert into users values(?,'',?,0,?,?,0,?,'',?,'',0);`, [
        user.email,
        user.name,
        user.pass,
        user.is_a_publisher,
        user.private_Q_id,
        user.private_ans,
      ], true).then(x => {
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }
  public login(email: string, pass: string): Promise<Users> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select * from users where email = ? and pass = ?`, [email, pass], true).then(x => {
        if (x.rows.length > 0) {
          resolve(x.rows[0]);
        } else resolve(null);
      }, (error) => {
        reject(error);
      });
    });
  }

   //################### search song simple (only by name)
   songSearchSimple(Song_name: string): Promise<Songs> {
    return new Promise<any>((resolve, reject) => {
      this.query('select * songs where songs.name=?', [Song_name]).then(x => {
        if (x.rows.length > 0) {
          resolve(x.rows[0]);
        } else resolve(null);
      }, (error) => {
        reject(error);
      });
    });
  }

   //################### search song advancly ( by name ,...)
  songSearchAdv(Song_name: string,maker_name: string,tag: string,lyrics:string,time_by_sec : number,release_time:number): Promise<Songs> {
    return new Promise<any>((resolve, reject) => {
      this.query('select * from songs where songs.name = ? and maker_name = ? and songs.tag = ? and songs.lyrics = ? and songs.time_by_sec < ? and songs.release_time =?', [Song_name,maker_name,tag,lyrics,time_by_sec,release_time]).then(x=> {
        if (x.rows.length > 0) {
          resolve(x.rows[0]);
        } else resolve(null);
      }, (error) => {
        reject(error);
      });
    });
  }

     //################### search album simple (only by name)
     albumSearchSimple(Album_name: string): Promise<Album> {
      return new Promise<any>((resolve, reject) => {
        this.query('select * album where album.name=?', [Album_name]).then(x => {
          if (x.rows.length > 0) {
            resolve(x.rows[0]);
          } else resolve(null);
        }, (error) => {
          reject(error);
        });
      });
    }

 //################### search album advancly ( by name ,...)
  albumSearchAdv(Album_name: string,tag: string,maker_name: string,release_time:number): Promise<Album> {
    return new Promise<any>((resolve, reject) => {
      this.query('select *  from album where album.name = ? and ? =EXISTS(select tag from album_tags ) and album.maker_name = ? and album.release_time =?', [Album_name,tag,maker_name,release_time]).then(x=> {
        if (x.rows.length > 0) {
          resolve(x.rows[0]);
        } else resolve(null);
      }, (error) => {
        reject(error);
      });
    });
  }


  //################### search user simple (only by name)
  userSearchSimple(User_name: string): Promise<Users> {
        return new Promise<any>((resolve, reject) => {
          this.query('select *  from users  where users.name  = ?', [User_name]).then(x => {
            if (x.rows.length > 0) {
              resolve(x.rows[0]);
            } else resolve(null);
          }, (error) => {
            reject(error);
          });
        });
      }
  

  //################### search user advance (only by username)
  userSearchadv(User_username: string): Promise<Users> {
    return new Promise<any>((resolve, reject) => {
      this.query('select *  from users  where users.username  = ?', [User_username]).then(x => {
        if (x.rows.length > 0) {
          resolve(x.rows[0]);
        } else resolve(null);
      }, (error) => {
        reject(error);
      });
    });
  }


  public getAllQuestions(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.query(`select * from private_Q_list;`, []).then(x => {
        resolve(Object.values(x.rows));
      });
    });
  }
  public addSong(song: Songs): Promise<void> {

    return new Promise<any>((resolve, reject) => {
      this.query(`insert into songs values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [
        song.song_id,
        song.tag,
        song.name,
        song.time_by_sec,
        song.lyrics,
        song.ahangsaz,
        song.shaaer,
        song.tahiekonande,
        song.maker_email,
        song.maker_name,
        song.maker_username,
        song.manager_email,
        song.album_id,
        1,
      ], true).then(x => {
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }

  public addAlbum(album: Album): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.query(`insert into album values (?,0,?,?,?,?,0)`, [
        album.album_id,
        album.name,
        album.maker_email,
        album.maker_name,
        album.maker_username,
      ], true).then(x => {
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }

  public addPlaylist(playlist: Playlist): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.query(`insert into playlist values (?,?,0,?,?,?,?)`, [
        playlist.playlist_id,
        playlist.playlist_name,
        playlist.private_public ? playlist.private_public : 0,
        playlist.maker_email,
        playlist.maker_name,
        playlist.maker_username,
      ], true).then(x => {
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }
  public getUserSongs(email: string): Promise<Songs[]> {
    return this.query(`select * from songs where maker_email = ?`, [email]).then((x) => {
      return Object.values<Songs>(x.rows);
    })
  }
  public getUserLikedSongs(email: string): Promise<Songs[]> {
    return this.query(`select songs from songs,liked_songs where liked_songs.user_email = ? and liked_songs.song_id = songs.song_id`, [email]).then((x) => {
      return Object.values<Songs>(x.rows);
    })
  }
  public getUserHistorySongs(email: string): Promise<Songs[]> {
    return this.query(`select * from songs where maker_email = ?`, [email]).then((x) => {
      return Object.values<Songs>(x.rows);
    })
  }
  public getUserPlaylists(email: string): Promise<Playlist[]> {
    return this.query(`select * from playlist where maker_email = ?`, [email], true).then((x) => {
      return Object.values<Playlist>(x.rows);
    })
  }
  public getUserAlbums(email: string): Promise<Album[]> {
    return this.query(`select * from album where maker_email = ?`, [email]).then((x) => {
      return Object.values<Album>(x.rows);
    })
  }

}


const tables: string[] = [
  `create table IF NOT EXISTS users(
    email char(50),
    username char(30),
    name char(30),
    picture int,
    pass char(30) check(length(pass) > 8), --(pass like '[a-z] %'), --or pass like '[0-9]%[a-z]%'), --and length(pass) > 8),
  is_a_publisher int,
  is_a_manager int,
  private_Q_id int,
  private_question char(50),
  private_ans char(50),
  manager_email char(50),
  now_playing_song_id int,
  Primary key(email),
  Foreign key(private_Q_id) references private_Q_list(private_id),
  Foreign key(now_playing_song_id) references songs(song_id),
  Foreign key(manager_email) references users(email)
);`,

  `create table IF NOT EXISTS user_fav_tags(
  user_email char(50),
  tag char(20),
  Foreign key(user_email) references users(email),
  Primary key(user_email, tag)
);`,

  `create table IF NOT EXISTS user_following(
  user_email char(50),
  following_user char(50),
  Foreign key(user_email) references users(email),
  Primary key(user_email, following_user)
);
`,
  `create table IF NOT EXISTS user_follower(
  user_email char(50),
  follower_user char(50),
  Foreign key(user_email) references users(email),
  Primary key(user_email, follower_user)
);`,

  `create table IF NOT EXISTS private_Q_list(
  private_id int,
  question char(100),
  Primary key(private_id)
);`,

  `create table IF NOT EXISTS liked_songs(
  user_email char(50),
  song_id int,
  Foreign key(user_email) references users(email),
  Foreign key(song_id) references songs(song_id),
  Primary key(user_email, song_id)
);`,


  `create table IF NOT EXISTS rated_songs(
  user_email char(50),
  song_id int,
  rate int,
  Foreign key(user_email) references users(email),
  Foreign key(song_id) references songs(song_id),
  Primary key(user_email, song_id)
);`,


  `create table IF NOT EXISTS rated_albums(
  user_email char(50),
  album_id int,
  rate int,
  Foreign key(user_email) references users(email),
  Foreign key(album_id) references album(album_id),
  Primary key(user_email, album_id)
);`,

  `create table IF NOT EXISTS rated_playlists(
  user_email char(50),
  playlist_id int,
  rate int,
  Foreign key(user_email) references users(email),
  Foreign key(playlist_id) references playlist(playlist_id),
  Primary key(user_email, playlist_id)
);`,

  `create table IF NOT EXISTS listened_songs(
  user_email char(50),
  song_id int,
  Foreign key(user_email) references users(email),
  Foreign key(song_id) references songs(song_id),
  Primary key(user_email, song_id)
);`,

  `create table IF NOT EXISTS songs(
  song_id int,
  tag char(20),
  name char(30),
  time_by_sec int,
  lyrics char(500),
  shaaer char(30),
  ahangsaz  char(30),
  tahiekonande  char(30),
  maker_email char(50),
  maker_name char(30),
  maker_username char(30),
  manager_email char(50),
  album_id int,
  release_time int,
  Primary key(song_id),
  Foreign key(maker_email) references users(email),
  Foreign key(manager_email) references users(email),
  Foreign key(album_id) references album(album_id)
);`,

  `create table IF NOT EXISTS playlist_songs(
  playlist_id int,
  song_id int,
  Foreign key(playlist_id) references playlist(playlist_id),
  Foreign key(song_id) references songs(song_id),
  Primary key(song_id, playlist_id)
);`,

  `create table IF NOT EXISTS album_tags(
  album_id int,
  tag char(20),
  Foreign key(album_id) references album(album_id),
  Primary key(album_id, tag)
);`,

  `create table IF NOT EXISTS album(
  album_id int,
  num_play int,
  name char(30),
  maker_email char(50),
  maker_name char(30),
  maker_username char(30),
  release_time int,
  Primary key(album_id),
  Foreign key(maker_email) references users(email)
);`,


  `create table IF NOT EXISTS playlist(
  playlist_id int,
  playlist_name char(30),
  re_time int,
  private_public int,
  maker_email char(50),
  maker_name char(30),
  maker_username char(30),
  Primary key(playlist_id),
  Foreign key(maker_email) references users(email)
);`,
]
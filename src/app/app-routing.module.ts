import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartPageComponent } from 'src/app/start-page/start-page.component';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { HomePageComponent } from 'src/app/home-page/home-page.component';
import { ProfilePageComponent } from 'src/app/profile-page/profile-page.component';
import { ManagmentPageComponent } from 'src/app/managment-page/managment-page.component';
import { MusicPageComponent } from 'src/app/music-page/music-page.component';
import { LibraryPageComponent } from 'src/app/library-page/library-page.component';
import { PlayListPageComponent } from 'src/app/play-list-page/play-list-page.component';
import { UsersComponent } from 'src/app/users/users.component';
import { AdvanceSearchComponent } from 'src/app/advance-search/advance-search.component';

const routes: Routes = [
  {
    path: 'login',
    component: StartPageComponent,
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        component: HomePageComponent,
      },
      {
        path: 'search',
        component:  AdvanceSearchComponent ,
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'music/:id',
        component:  MusicPageComponent ,
      },
      {
        path: 'library/:id',
        component: LibraryPageComponent,
      },
      {
        path: 'playList/:id',
        component: PlayListPageComponent,
      },
      {
        path: 'profile',
        component: ProfilePageComponent,
      },
      {
      path: 'manage',
      component: ManagmentPageComponent,
      },
    ]
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Songs } from '../songs';
import { SongEditDialogComponent } from '../song-edit-dialog/song-edit-dialog.component';
import { MusicService } from '../music.service';
import { AuthService } from '../auth.service';
import { Playlist } from '../playlist';
import { Album } from '../album';
import { AlbumEditDialogComponent } from '../album-edit-dialog/album-edit-dialog.component';
import { PlaylistEditDialogComponent } from '../playlist-edit-dialog/playlist-edit-dialog.component';
import { SocialService } from '../social.service';
import { Users } from '../users';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {
  songs: Songs[];
  playlists: Playlist[];
  albums: Album[];
  likedSongs: Songs[];
  historySongs: Songs[];
  followers = 0;
  followings = 0;
  user: Users;

  constructor(
    private dialog: MatDialog,
    private music: MusicService,
    private snack: MatSnackBar,
    private auth: AuthService,
    private social: SocialService,
  ) { }

  ngOnInit() {
    this.songs = [];
    this.music.getUserSongs(this.auth.user.email).then((songs) => {
      this.songs = songs;
    });
    this.music.getUserAlbums(this.auth.user.email).then((albums) => {
      this.albums = albums;
    });
    this.music.getUserPlaylists(this.auth.user.email).then((playlists) => {
      this.playlists = playlists;
    });
    this.social.getFollowersCount(this.auth.user.email).then((count) => {
      this.followers = count;
    });
    this.social.getFollowingCount(this.auth.user.email).then((count) => {
      this.followings = count;
    });
  }

  openSongDialog(song?: Songs) {
    let ref = this.dialog.open(SongEditDialogComponent, {
      direction: 'ltr',
    });
    ref.afterClosed().subscribe((song) => {
      console.log(1);
      this.music.addSong(song).then(() => {
        this.snack.open('موسیقی با موفقیت درج شد', '', {
          duration: 3000
        });
        this.songs.push(song);
      });
    })
  }

  openAlbumDialog(album?: Album) {
    let ref = this.dialog.open(AlbumEditDialogComponent, {
      direction: 'ltr',
    });
    ref.afterClosed().subscribe((album) => {
      console.log(1);
      this.music.addAlbum(album).then(() => {
        this.snack.open('آلبوم با موفقیت درج شد', '', {
          duration: 3000
        });
        this.albums.push(album);
      });
    })
  }

  openPlaylistDialog(playlist?: Playlist) {
    let ref = this.dialog.open(PlaylistEditDialogComponent, {
      direction: 'ltr',
    });
    ref.afterClosed().subscribe((playlist) => {
      this.music.addPlaylist(playlist).then(() => {
        this.snack.open('پلی لیست با موفقیت درج شد', '', {
          duration: 3000
        });
        this.playlists.push(playlist);
      });
    })
  }

}

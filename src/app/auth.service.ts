import { Injectable } from '@angular/core';
import { Users } from 'src/app/users';
import { DbConnectionService } from 'src/app/db-connection.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Users;
  constructor(private db: DbConnectionService) { }

  register(user: Users): Promise<Users> {
    return new Promise((resolve, reject) => {
      this.db.register(user).then(() => {
        this.user = user;
        resolve(this.user);
      }, (error) => {
        reject(error);
      });
    });
  }

  login(email: string, pass: string): Promise<Users> {
    return this.db.login(email, pass).then((res) => {
      if (res) {
        this.user = res;
      }
      return res;
    })
  }
}
